This is a C# submission for the BT Technology Internship Software Coding Exercise.

It is a solution to be opened in VisualStudio Express 2013 for Windows Desktop as requested in the guidelines.

Once opened the program can be built and run by pressing the start button.

After this, a command line can be opened in the SuperheroesInc\bin\Debug folder.
The program can then be invoked by calling it using for example the following instruction:
SuperheroesInc superheroes.txt "Batman" "Super Ted"

A sample of .txt files have been included which contain a variety of organisational charts that were used for testing.