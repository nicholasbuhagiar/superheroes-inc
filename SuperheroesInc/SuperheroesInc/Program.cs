﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SuperheroesInc
{
    class Program
    {
        // Boolean which holds whether or not an employee has been found
        // Global so it does now need to be passed up through the recursion
        private static Boolean employeeTwoFound;

        // List of employees in the system
        // Global so that it does not need to be passed to the large range of functions that use it
        private static List<Employee> employeeList;

        // List of employees used in the system
        // Copy of the regular list, but this one is used to change the values and can then be restored
        // Global so that it does not need to be passed a lot
        private static List<Employee> editableEmployeeList;

        static void Main(string[] args)
        {
            Boolean validArguments = false;

            // Checks that the correct number of arguments were given to the program
            // If it's 3 then the program will continue, if more or less then it will terminate
            //Console.WriteLine("Number of parameters = " + args.Length);
            if (args.Length == 3)
            {
                //Console.WriteLine("Correct number of arguments input");
                validArguments = true;
            }
            else
            {
                //Console.WriteLine("Incorrect number of arguments input");
                validArguments = false;
            }

            if (validArguments)
            {
                // Load the emplotyees from the given .txt file
                employeeList = LoadEmployees(args[0]);

                editableEmployeeList = new List<Employee>();

                // Copy the employee list into another list that can be editted
                // This is needed so that the program can loop and find routes between the employees with the same name
                for (int i = 0; i < employeeList.Count(); i++)
                {
                    Employee tempEmployee = new Employee(employeeList[i].EmployeeID, employeeList[i].EmployeeName, employeeList[i].ManagerID);

                    editableEmployeeList.Add(tempEmployee);
                }

                // Get the employee names from the arguments
                string employeeOneName = args[1];
                string employeeTwoName = args[2];

                // Format the name
                // This removes blank space and makes it all lower case so they can be cmpared later
                employeeOneName = FormatName(employeeOneName);
                employeeTwoName = FormatName(employeeTwoName);

                // Find how many employees have the names one and two
                // This is needed so the program can loop and get all the routes between employees with the relevant names
                int numberEmployeesWithNameOne = NumberEmployeesWithName(employeeOneName);
                int numberEmployeesWithNameTwo = NumberEmployeesWithName(employeeTwoName);

                // Instatiate a list that will hold which employees names have been set to blank so they can be replaced when needed
                List<int> employeesChanged = new List<int>();

                // Loop for the number of employees with name one
                for (int i = 0; i < numberEmployeesWithNameOne; i++)
                {
                    // This will reset the names of any employee whose name has been removed as to find multiple employees with the same name
                    for (int j = 0; j < employeesChanged.Count(); j++)
                    {
                        // If the two employees have the same name then we want to keep the first instance of the name blank
                        // Otherwise duplicates will be produced
                        if (employeeOneName.Equals(employeeTwoName))
                        {
                            if (j > 0)
                            {
                                SetNameByID(employeesChanged[j], employeeTwoName);
                            }
                        }
                        else
                        {
                            SetNameByID(employeesChanged[j], employeeTwoName);
                        }
                    }

                    // Find an employee with the relevant name
                    Employee employeeOne = FindEmployee(employeeOneName);

                    employeesChanged = new List<int>();

                    // Loop for the number of employees with name two
                    for (int j = 0; j < numberEmployeesWithNameTwo; j++)
                    {
                        employeeTwoFound = false;

                        Employee employeeTwo = FindEmployee(employeeTwoName);

                        if (employeeOne.EmployeeID != employeeTwo.EmployeeID)
                        {
                            // Find the link between employee one and employee two
                            // string returned contains employee ID's and arrows (<,>)
                            string link = FindLink(employeeOne, employeeTwo);

                            // Format the link to display the correct line
                            link = FormatLink(link);

                            // Output the result
                            Console.WriteLine(link);
                        }

                        // Set the employee name who was just found to blank
                        // This allows a route between employee one and multiple employee two's to be found
                        for (int x = 0; x < employeeList.Count(); x++)
                        {
                            if (employeeTwo.EmployeeID == employeeList[x].EmployeeID)
                            {
                                editableEmployeeList[x].EmployeeName = "";
                                employeesChanged.Add(employeeTwo.EmployeeID);

                                // break as the relevant employee has been found
                                // So we can save cpu cycles by leaving the for loop here
                                break;
                            }
                        }
                    }

                    // If the two employees have the same name then we want to subtract one each time as do not want duplicates
                    if (employeeOneName.Equals(employeeTwoName))
                    {
                        numberEmployeesWithNameTwo--;
                    }

                    // Set employee one name who was just used to blank
                    // Allows for multiple employees with name one
                    for (int x = 0; x < employeeList.Count(); x++)
                    {
                        if (employeeOne.EmployeeID == employeeList[x].EmployeeID)
                        {
                            editableEmployeeList[x].EmployeeName = "";
                            break;
                        }
                    }
                }
            }

            //Console.ReadLine();
        }

        /// <summary>
        /// Sets the name of the given employee, by the ID, to the given name
        /// </summary>
        /// <param name="ID">Integer holding the ID of the employee whose name needs to be set</param>
        /// <param name="name">string holding the name to be set</param>
        private static void SetNameByID(int ID, string name)
        {
            for (int i = 0; i < employeeList.Count(); i++)
            {
                if (employeeList[i].EmployeeID == ID)
                {
                    editableEmployeeList[i].EmployeeName = name;
                    break;
                }
            }
        }

        /// <summary>
        /// Counts the number employees who have the given name
        /// </summary>
        /// <param name="employeeName">Name to search for</param>
        /// <returns>Integer containing the number of employees with the given name</returns>
        private static int NumberEmployeesWithName(string employeeName)
        {
            int counter = 0;

            // Goes through the employee list looking for employees with the given name
            for (int i = 0; i < employeeList.Count(); i++)
            {
                // Checks if an the current employee name equals the search name
                // The current employee name is converted to lowercase so it will match
                if (employeeName.Equals(employeeList[i].EmployeeName.ToLower()))
                {
                    counter++;
                }
            }

            return counter;
        }

        /// <summary>
        /// Formats the given link to the correct format
        /// Adds in the employee names and brackets and correct arrows
        /// </summary>
        /// <param name="link">string holding the link to format</param>
        /// <returns>string with the correct formatting</returns>
        private static string FormatLink(string link)
        {
            // Splits the string around the right arrows
            string[] splitLink = link.Split('>');
            string completeLink = "";

            // Rebuilds the string adding in the employee names
            for (int i = 0; i < (splitLink.Count() - 1); i++)
            {
                completeLink = completeLink + GetEmployeeNameByID(splitLink[i]) + " (" + splitLink[i] + ") -> ";
            }

            // Splits the remainder of the string around the left arrow
            splitLink = splitLink[splitLink.Count() - 1].Split('<');

            // Rebuilds the rest of the string with employee names
            for (int i = 0; i < splitLink.Count(); i++)
            {
                completeLink = completeLink + GetEmployeeNameByID(splitLink[i]) + " (" + splitLink[i] + ")";

                if (i < (splitLink.Count() - 1))
                {
                    completeLink = completeLink + " <- ";
                }
            }

            return completeLink;
        }

        /// <summary>
        /// Gets the name of the employee with the given ID
        /// </summary>
        /// <param name="employeeIDString">ID of employee whose name needs to be found</param>
        /// <returns>string containing the name of the wanted employee</returns>
        private static string GetEmployeeNameByID(string employeeIDString)
        {
            int employeeID = Convert.ToInt32(employeeIDString);
            string employeeName = "";

            // Search through the employee list for the employee with the given ID and return the name
            for (int i = 0; i < employeeList.Count(); i++)
            {
                if (employeeList[i].EmployeeID == employeeID)
                {
                    employeeName = employeeList[i].EmployeeName;
                }
            }

            return employeeName;
        }

        /// <summary>
        /// Loads the employees from the given .txt file
        /// </summary>
        /// <param name="employeeLocation">string holding the name of the .txt file to be read</param>
        /// <returns>List of employees got from the .txt file</returns>
        private static List<Employee> LoadEmployees(string employeeLocation)
        {
            List<Employee> employeeList;
            String line;
            StreamReader streamReader = new StreamReader(employeeLocation);
            Employee newEmployee;
            int counter = 0;

            employeeList = new List<Employee>();

            // Read the file until the end is reached
            while ((line = streamReader.ReadLine()) != null)
            {
                //Console.WriteLine(line);

                // Counter needs to be greater than 0 so that it ignores the first line with the titles
                if (counter > 0)
                {
                    // Split around the "|" to get individual elements
                    string[] splitLine = line.Split('|');
    
                    // Trim to remove blank space
                    string employeeIDString = splitLine[1].Trim();

                    // If the current line has no employee number then it is assumed this employee has left so ignores the line
                    // Otherwise it reads the rest of this line
                    if (employeeIDString != "")
                    {
                        int employeeID = Convert.ToInt32(employeeIDString);

                        string employeeName = splitLine[2].Trim();

                        string managerIDString = splitLine[3].Trim();

                        // If the manager ID is blank then this person is at the top
                        if (managerIDString == "")
                        {
                            // So new Employee is created with just 2 arguments
                            // The manager ID of this employee with be set to a default of 0
                            newEmployee = new Employee(employeeID, employeeName);
                        }
                        else
                        {
                            int managerID = Convert.ToInt32(managerIDString);

                            // New employee is created with 3 arguments
                            newEmployee = new Employee(employeeID, employeeName, managerID);
                        }

                        // Employee is added to the list
                        employeeList.Add(newEmployee);
                    }
                }
                counter++;
            }

            streamReader.Close();

            return employeeList;
        }

        /// <summary>
        /// Format the given string name
        /// </summary>
        /// <param name="employeeName">string holding the name to be formatted</param>
        /// <returns>string holding the formatted name</returns>
        private static string FormatName(string employeeName)
        {
            // If multiple blank spaces are in the string, these are replaced with one space
            while (employeeName.Contains("  "))
            {
                employeeName = employeeName.Replace("  ", " ");
            }

            // Convert the name to lowercase so it can be compared later
            employeeName = employeeName.ToLower();

            return employeeName;
        }

        /// <summary>
        /// Find the link between the two given employees
        /// </summary>
        /// <param name="employeeOne">Employee that the connection is going from</param>
        /// <param name="employeeTwo">Employee that the connection is going to</param>
        /// <returns>string containing the link between the two employees</returns>
        private static string FindLink(Employee employeeOne, Employee employeeTwo)
        {
            string link = "";

            //Console.WriteLine(employeeOne.EmployeeName);
            //Console.WriteLine(employeeTwo.EmployeeName);            

            int depth = 0;

            // Find the depth of employee one
            depth = FindDepth(employeeOne);

            //Console.WriteLine(depth);

            Employee currentEmployee = employeeOne;
            // Start to construct the string that holds the link
            link = employeeOne.EmployeeID.ToString();

            // First search the "children" of the current employee
            // Give the search function a 0 parameter at the end so that it does not skip any of the children
            link = SearchChildren(employeeOne, employeeTwo, link, 0);

            for (int i = 0; i < depth; i++)
            {
                if (!employeeTwoFound)
                {
                    // Go up one step in the management chain
                    Employee manager = FindManager(currentEmployee);
                                        
                    link = link + ">" + manager.EmployeeID;

                    // If this manager isn't the employee being looked for then search its children
                    // Otherwise break as employee two has been found
                    if (manager.EmployeeID != employeeTwo.EmployeeID)
                    {
                        // Give the search function the current employee ID at the end so that it skips this employee
                        // This is done as that employee with already have its children searched
                        // So save on processing by not searching them again
                        link = SearchChildren(manager, employeeTwo, link, currentEmployee.EmployeeID);
                    }
                    else
                    {
                        break;
                    }

                    //  Set the current manager as the new current employee, so you can go up another step in the chain on the next loop
                    currentEmployee = manager;
                }
            }

            return link;
        }

        /// <summary>
        /// Search the children of the given manager for the employee we are looking for
        /// </summary>
        /// <param name="manager">Employee that is the manager to search the children of</param>
        /// <param name="employeeTwo">Employee that is to be found</param>
        /// <param name="link">string holding the current link between the employees</param>
        /// <param name="employeeToSkip">integer holding the ID of an employee to skip, if this employee has already been searched we don't need to do it again</param>
        /// <returns>string holding the new link between employees</returns>
        private static string SearchChildren(Employee manager, Employee employeeTwo, string link, int employeeToSkip)
        {
            // Link is copied so that it can be added to, and only commited if the employee is found
            string tempLink = link;
            // Finds the "children" of the current manager
            List<Employee> children = FindChildren(manager);

            // Go through the list of children
            for (int i = 0; i < children.Count; i++)
            {
                // If this employee should be skipped (as it has already been searched) then it will not be run
                if (children[i].EmployeeID != employeeToSkip)
                {
                    tempLink = link;

                    // Add the employee ID to the temporary link string
                    tempLink = tempLink + "<" + children[i].EmployeeID;

                    // If the child employee ID is equal to the ID we are searching for then the employee has been found
                    if (children[i].EmployeeID == employeeTwo.EmployeeID)
                    {
                        //Console.WriteLine("Employee Two Found");

                        // We can proceed to commit the changes to the link and leave the for loop
                        link = tempLink;
                        employeeTwoFound = true;
                        break;
                    }
                    else
                    {
                        // If the child was not the emnployee we are looking for, then we need to search its children

                        // Give function a 0 at the last parameter so that it skips none of the children
                        tempLink = SearchChildren(children[i], employeeTwo, tempLink, 0);

                        // Check if the employee whose ID we are looking for has been added to the end of the temporary link
                        // Then we can commit the changes as the employee has been found
                        // And break to save unnecessary processing
                        string[] linkSplit = tempLink.Split('<');
                        if (linkSplit[linkSplit.Length - 1].Equals(employeeTwo.EmployeeID.ToString()))
                        {
                            link = tempLink;
                            break;
                        }
                    }
                }
            }

            return link;
        }

        /// <summary>
        /// Find the "children" (employees under the given employee)
        /// </summary>
        /// <param name="employee">Employee to find children of</param>
        /// <returns>List of Emplyees who are the children of the given employee</returns>
        private static List<Employee> FindChildren(Employee employee)
        {
            List<Employee> childrenList = new List<Employee>();

            for (int i = 0; i < employeeList.Count(); i++)
            {
                if (employeeList[i].ManagerID == employee.EmployeeID)
                {
                    childrenList.Add(employeeList[i]);
                }
            }

            return childrenList;
        }

        /// <summary>
        /// Find the employee who has the given name
        /// </summary>
        /// <param name="employeeName">string holding the employee name</param>
        /// <returns>Employee who has been found</returns>
        private static Employee FindEmployee(string employeeName)
        {
            Employee employee = new Employee();

            for (int i = 0; i < employeeList.Count(); i++)
            {
                if (employeeName.Equals(editableEmployeeList[i].EmployeeName.ToLower()))
                {
                    employee = editableEmployeeList[i];
                    break;
                }
            }

            return employee;
        }

        /// <summary>
        /// Find how deep the employee is in the structure
        /// Uses recursion to keep looping until it reaches the top, then adds one each time as it unwinds
        /// </summary>
        /// <param name="employee">Employee to find</param>
        /// <returns>Integer holding the depth of the given employee</returns>
        private static int FindDepth(Employee employee)
        {
            // If manager ID is equal to 0, then this person is at the top on the structure
            // So return 0 and the recursion unwinds
            // Otherwise get the employee who is the manager and rerun this function
            if (employee.ManagerID == 0)
            {
                return 0;
            }
            else
            {
                Employee manager = new Employee();

                for (int i = 0; i < employeeList.Count(); i++)
                {
                    if (employee.ManagerID == employeeList[i].EmployeeID)
                    {
                        manager = employeeList[i];
                        break;
                    }
                }

                // Uses recursion and gives it the manager of the current employee
                return (FindDepth(manager) + 1);
            }
        }

        /// <summary>
        /// Finds the employee who is the manager of the given employee
        /// </summary>
        /// <param name="currentEmployee">Employee to find manager of</param>
        /// <returns>Employee who is the manager</returns>
        private static Employee FindManager(Employee currentEmployee)
        {
            Employee manager = new Employee();

            // Go through the list until the employee who has the same ID as the given employees manager ID is found
            for (int i = 0; i < employeeList.Count(); i++)
            {
                if (currentEmployee.ManagerID == employeeList[i].EmployeeID)
                {
                    manager = employeeList[i];
                    break;
                }
            }

            return manager;
        }
    }
}
