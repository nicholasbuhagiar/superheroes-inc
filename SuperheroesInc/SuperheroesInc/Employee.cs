﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperheroesInc
{
    /// <summary>
    /// Class to hold information about an Employee of a company
    /// </summary>
    class Employee
    {
        // Variables holding the employees ID, name and their managers ID
        private int employeeID;
        private string employeeName;
        private int managerID;

        /// <summary>
        /// Defualt constructor for the class
        /// </summary>
        public Employee()
        {
            
        }

        /// <summary>
        /// Constructor when given 2 arguments
        /// This will be the person at the top of the system who has no manager
        /// Therefore, their managerID will be set to 0
        /// </summary>
        /// <param name="employeeID">integer holding the ID of this employee</param>
        /// <param name="employeeName">string holding the name of this employee</param>
        public Employee(int employeeID, string employeeName)
        {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
            this.managerID = 0;
        }

        /// <summary>
        /// Constructor when given 3 arguments
        /// </summary>
        /// <param name="employeeID">integer holding the ID of this employee</param>
        /// <param name="employeeName">string holding the name of this employee</param>
        /// <param name="managerID">integer holding the employee ID of the manager of this employee</param>
        public Employee(int employeeID, string employeeName, int managerID)
        {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
            this.managerID = managerID;
        }

        /// <summary>
        /// Function for accessing the private employeeID variable
        /// This value can only be got once set initially
        /// </summary>
        public int EmployeeID
        {
            get
            {
                return employeeID;
            }
        }

        /// <summary>
        /// Function for accessing the private employeeName variable
        /// This variable can be got and set
        /// </summary>
        public string EmployeeName
        {
            get
            {
                return employeeName;
            }

            set
            {
                employeeName = value;
            }
        }

        /// <summary>
        /// Function for accessing the private managerID variable
        /// This cariable can only be got
        /// </summary>
        public int ManagerID
        {
            get
            {
                return managerID;
            }
        }

    }
}
